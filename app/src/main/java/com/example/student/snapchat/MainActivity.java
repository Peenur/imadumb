package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "618274B2-5E3D-F835-FF0E-E697CF27CE00";
    public static final String SECRET_KEY = "6BCBBA25-6519-5E41-FF74-32CFD9DF3300";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //MainMenuFragment mainMenu = new MainMenuFragment();
        //getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();

        LoginMenuFragment mainMenu = new LoginMenuFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
    }
}